FROM python:2.7-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ADD django/requirements.txt /requirements.txt

RUN set -ex \
    && apk add --no-cache mariadb-connector-c-dev \
    && apk add --no-cache --virtual .build-deps \
            gcc \
	    g++ \
            make \
            libc-dev \
            musl-dev \
            linux-headers \
            pcre-dev \
	    jpeg-dev \
            py2-pip \
            libffi-dev \
            mariadb-dev \     
    && pip install --no-cache-dir -r /requirements.txt \
    && runDeps="$( \
            scanelf --needed --nobanner --recursive /venv \
                    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                    | sort -u \
                    | xargs -r apk info --installed \
                    | sort -u \
    )" \
    && apk add --virtual .python-rundeps $runDeps \
    && apk del .build-deps


RUN mkdir /app/
WORKDIR /app/
ADD django/ /app/
RUN mkdir -p /app/log && chown -R 1000:2000 /app/log \
    && mkdir -p /app/cache && chown -R 1000:2000 /app/cache \
    && mkdir -p /app/media && chown -R 1000:2000 /app/media  \
    && mkdir -p /app/tmp && chown -R 1000:2000 /app/tmp \
    && mkdir -p /app/static && chown -R 1000:2000 /app/static

EXPOSE 8000
USER 1000:2000

RUN DATABASE_URL=none python manage.py collectstatic --noinput

CMD ["/usr/local/bin/gunicorn", "-b", "0.0.0.0:8000", "--access-logfile", "-", "totmann.wsgi"]
