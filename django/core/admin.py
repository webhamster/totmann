from django.contrib import admin
from .models import Check, CheckEvent, NotificationReceiver

class CheckAdmin(admin.ModelAdmin):
    list_display = ('enabled', 'owner', 'prefix', 'name', 'interval')

admin.site.register(Check, CheckAdmin)
admin.site.register(CheckEvent)
admin.site.register(NotificationReceiver)

