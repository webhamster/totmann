#!/bin/sh

set -e

cd /app

while true; do
    python manage.py run_checks
    sleep 30
done
